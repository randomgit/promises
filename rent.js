const requiredRent = 7500;

let amount = 9500;

const promise = new Promise((resolve, reject) => {
  if (amount > requiredRent) {
    setTimeout(() => {
      console.log("The landlord is will be waiting for his rent! Your amount of money is " + amount);
    }, 3000)

    setTimeout(() => {
      amount -= requiredRent;
      console.log("You're rent is checking in.");
    }, 5000)

    setTimeout(() => {
      resolve("The rent has been paid! You will be able to stay for one more month before it expires! Your amount is now " + amount);
    }, 10000)
  } else {
    reject("You do not have enough money to stay in this house!");
  }
});

promise.then(
  value => {
    console.log(value);
  },
  error => {
    console.log(error);
  }
)