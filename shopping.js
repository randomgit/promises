let money = 15000;

const budget = {
  _tomato: 45,
  _bread: 50,
  _Jam: 34,
  _flour: 55,

  get tomato() {
    return this._tomato;
  },

  get bread() {
    return this._bread;
  },

  get Jam() {
    return this._Jam;
  },

  get flour() {
    return this._flour;
  },

  buyAllStuff() {
    money -= allBudg;
  }
}

const allBudg = budget.tomato + budget.bread + budget.Jam + budget.flour;

const promise = new Promise((resolve, reject) => {
  if (money > allBudg) {
    setTimeout(() => {
      console.log("Waiting for you're turn to pay and claim you're stuff.");
    }, 3000)

    setTimeout(() => {
      console.log("Checking all your supplies...");
    }, 5000)

    setTimeout(() => {
      budget.buyAllStuff();
      console.log("Bought all stuff! Your money is now " + money);
    }, 10000)
  } else {
    reject("If you do not have money, do not bother come here to get stuff!");
  }
})

promise.then(
  value => {
    console.log(value);
  },
  error => {
    console.log(error);
  }
)