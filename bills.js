const requiredBills = 15.00;

const billsPaid = false;

const promise = new Promise((resolve, reject) => {
  if (billsPaid) {
    setTimeout(() => {
      console.log("The bills are about to get paid.");
    }, 1500 + 1500)

    setTimeout(() => {
      console.log("Sending the bills...");
    }, 2500 + 2500)

    setTimeout(() => {
      resolve("The bills have been paid, including the bills for water.");
    }, 5000 + 5000)
  } else {
    reject("You have not been paying your bills for almost 5 months now, the electricity and the water has been shut off!");
  }
})

promise.then(
  value => {
    console.log(value);
  },

  error => {
    console.log(error);
  }
)